package net.mahes.fact;

import static net.mahes.fact.ByteArrayTools.string2byteArray;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import net.mahes.fact.solver.PartialSolution;
import net.mahes.fact.solver.Solver;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String rsa1024 = "135066410865995223349603216278805969938881475605667027524485143851526510604859533833940287150571909441798207282164471551373680419703964191743046496589274256239341020864383202110372958725762358509643110564073501508187510676594629205563685529475213500852879416377328533906109750544334999811150056977236890927563";

		// String rsa100 =
		// "1522605027922533360535618378132637429718068114961380688657908494580122963258952897654000350692006139";
		// String test = "12345";

		String todo = rsa1024;

		byte[] target = string2byteArray(todo);

		// Solver solver = new Solver(target);
		// solver.init();
		Solver solver = loadFromFile(target, "1345698562986.txt");

		final long DUMP_TIMEOUT = 1000 * 60 * 60;
		long lastDump = -1;

		while (true) {
			runMultithreaded(solver, 8, 100000);
			System.out.println("Open Calculations: " + solver.openCalculations());
			System.out.println("Solutions: " + solver.solutions());

			if (lastDump < System.currentTimeMillis() - DUMP_TIMEOUT) {
				System.out.println("Dumping results ...");
				dump(solver);
				lastDump = System.currentTimeMillis();
				System.out.println("Next dump in " + (DUMP_TIMEOUT / 60 / 1000) + " minutes!");
			}
		}

	}

	private static Solver loadFromFile(byte[] target, String filename) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));

			ArrayList<PartialSolution> possibleSolutions = new ArrayList<>();
			ArrayList<PartialSolution> solutions = new ArrayList<>();

			String n1, n2, r;
			while (true) {
				n1 = reader.readLine();
				n2 = reader.readLine();
				r = reader.readLine();

				if (r == null)
					break;

				PartialSolution psol = new PartialSolution(
						ByteArrayTools.string2byteArray(n1.substring(5)),
						ByteArrayTools.string2byteArray(n2.substring(5)),
						ByteArrayTools.string2byteArray(r.substring(5)));

				boolean fixed = n1.startsWith("F");
				if (fixed) {
					solutions.add(psol);
				} else {
					possibleSolutions.add(psol);
				}
			}

			return new Solver(target, possibleSolutions, solutions);

		} catch (Exception e) {
			throw new Error(e);
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
			}
		}
	}

	private static void dump(Solver solver) {
		OutputStreamWriter writer = null;
		try {
			writer = new OutputStreamWriter(new FileOutputStream(System.currentTimeMillis() + ".txt"));
			solver.dumpSolutions(writer);
		} catch (Exception e) {

		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
			}
		}
	}

	private static void runMultithreaded(Solver solver, int threads, int stepsToDo) {
		System.out.println("Splitting solver ...");
		ArrayList<Solver> solvers = solver.splitYourself(threads);
		Thread[] solveThreads = new Thread[threads];

		System.out.println("Starting threads ...");
		for (int i = 0; i < threads; ++i) {
			solveThreads[i] = new Thread(new SolveThread(solvers.get(i), stepsToDo));
			solveThreads[i].start();
		}

		System.out.println("Waiting for threads ...");
		for (int i = 0; i < threads; ++i) {
			boolean done = false;
			while (!done) {
				try {
					solveThreads[i].join();
					done = true;
				} catch (InterruptedException e) {
				}
			}
		}

		System.out.println("Joining solvers ...");
		solver.append(solvers);
	}

	static class SolveThread implements Runnable {

		private Solver solver;
		private int stepsToDo;

		public SolveThread(Solver solver, int stepsToDo) {
			this.solver = solver;
			this.stepsToDo = stepsToDo;
		}

		@Override
		public void run() {
			for (int i = 0; i < stepsToDo; ++i) {
				if (!solver.step()) {
					return;
				}
			}
		}

	}

}
