package net.mahes.fact;

public class ByteArrayTools {

	public static String byteArray2String(byte[] input) {
		StringBuilder res = new StringBuilder();
		for (int i = input.length-1; i >= 0; --i) {
			res.append(input[i]);
		}
		return res.toString();
	}
	
	public static byte[] string2byteArray(String number) {
		byte[] result = new byte[number.length()];
		
		for (int i = 0, j = number.length()-1; i < number.length(); ++i, --j) {
			int nr = number.charAt(j) - '0';
			if (nr < 0 || nr > 9) {
				throw new RuntimeException("Invalid number string! Contais: " + number.charAt(j));
			}
			result[i] = (byte) nr;
		}
		
		return result;
	}
}
