package net.mahes.fact.solver;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static net.mahes.fact.ByteArrayTools.*;

public class Solver {

	private byte[] target;
	private ArrayList<PartialSolution> possibleSolutions;
	private ArrayList<PartialSolution> solutions;

	public Solver(byte[] target) {
		this(target, new ArrayList<PartialSolution>());
	}

	public Solver(byte[] target, ArrayList<PartialSolution> possibleSolutions) {
		this(target, possibleSolutions, new ArrayList<PartialSolution>());
	}

	public Solver(byte[] target, ArrayList<PartialSolution> possibleSolutions,
			ArrayList<PartialSolution> solutions) {
		assert (target != null);
		assert (possibleSolutions != null);
		assert (solutions != null);

		this.target = target;
		this.possibleSolutions = possibleSolutions;
		this.solutions = solutions;
	}

	private void addPossibleSolution(PartialSolution psol) {
		if (!possibleSolutions.contains(psol)) {
			possibleSolutions.add(psol);
		}
	}

	public ArrayList<Solver> splitYourself(int parts) {
		Collections.sort(possibleSolutions);

		ArrayList<Solver> res = new ArrayList<>(parts);

		for (int i = 0; i < parts; ++i) {
			res.add(new Solver(target));
		}

		while (!possibleSolutions.isEmpty()) {
			for (Solver solver : res) {
				if (possibleSolutions.isEmpty()) {
					break;
				}
				solver.possibleSolutions.add(possibleSolutions
						.remove(possibleSolutions.size() - 1));
			}
		}

		res.get(0).solutions.addAll(solutions);

		solutions.clear();
		possibleSolutions.clear();

		return res;
	}

	public void append(ArrayList<Solver> parts) {
		for (Solver s : parts) {
			for (PartialSolution psol : s.possibleSolutions) {
				addPossibleSolution(psol);
			}
			solutions.addAll(s.solutions);

			s.possibleSolutions.clear();
			s.solutions.clear();
		}
	}

	public boolean isSolutionReady() {
		return !solutions.isEmpty();
	}

	public int openCalculations() {
		return possibleSolutions.size();
	}

	public int solutions() {
		return solutions.size();
	}

	public void dumpSolutions(Writer writer) throws IOException {
		Collections.sort(possibleSolutions);
		Collections.sort(solutions);
		for (PartialSolution solution : solutions) {
			writer.write("FN1: " + byteArray2String(solution.number1) + "\r\n");
			writer.write("FN2: " + byteArray2String(solution.number2) + "\r\n");
			writer.write("FR:  " + byteArray2String(solution.result) + "\r\n");
		}
		for (PartialSolution solution : possibleSolutions) {
			writer.write("ON1: " + byteArray2String(solution.number1) + "\r\n");
			writer.write("ON2: " + byteArray2String(solution.number2) + "\r\n");
			writer.write("OR:  " + byteArray2String(solution.result) + "\r\n");
		}
	}

	public void printSolutions() {
		System.out.println("Fixed: ");
		for (PartialSolution solution : solutions) {
			System.out.println("N1: " + byteArray2String(solution.number1));
			System.out.println("N2: " + byteArray2String(solution.number2));
			System.out.println("R:  " + byteArray2String(solution.result));
			System.out.println();
		}
		System.out.println();
		System.out.println();
		System.out.println("Open: ");
		for (PartialSolution solution : possibleSolutions) {
			System.out.println("N1: " + byteArray2String(solution.number1));
			System.out.println("N2: " + byteArray2String(solution.number2));
			System.out.println("R:  " + byteArray2String(solution.result));
			System.out.println();
		}
	}

	public boolean init() {
		byte[] t = new byte[2];

		for (byte x = 0; x < 10; ++x) {
			for (byte y = 0; y < 10; ++y) {
				int p = x * y;
				t[0] = (byte) (p % 10);
				t[1] = (byte) (p / 10);
				if (doesFitTarget(t, 0, 1)) {
					addPossibleSolution(new PartialSolution(new byte[] { x },
							new byte[] { y }, t));

					t = new byte[2];
				}
			}
		}

		return false;
	}

	public boolean step() {
		if (possibleSolutions.isEmpty()) {
			return false;
		}
		PartialSolution oldSolution = possibleSolutions
				.remove(possibleSolutions.size() - 1);
		List<PartialSolution> newSolutions = calculationStep(oldSolution);
		for (PartialSolution solution : newSolutions) {
			if (solution.significantDigits() >= target.length) {
				synchronized (solutions) {
					solutions.add(solution);
				}
			} else {
				addPossibleSolution(solution);
			}
		}

		return true;
	}

	private List<PartialSolution> calculationStep(PartialSolution psol) {
		LinkedList<PartialSolution> newSolutions = new LinkedList<>();

		PartialSolution newSolution;

		for (byte x = 0; x < 10; ++x) {
			for (byte y = 0; y < 10; ++y) {
				newSolution = nextEmptyPartialSolution(psol);
				calcNextSolution(newSolution, x, y);
				if (doesFitTarget(newSolution.result,
						newSolution.significantDigits() - 1,
						newSolution.significantDigits())) {
					newSolutions.add(newSolution);
				}
			}
		}

		return newSolutions;
	}

	private void calcNextSolution(PartialSolution psol, byte na1, byte na2) {
		psol.number1[psol.number1.length - 1] = na1;
		psol.number2[psol.number2.length - 1] = na2;

		int pos = (psol.number1.length - 1) + (psol.number2.length - 1);
		int pres = na1 * na2;

		psol.result[pos] += (byte) pres;
		clearOverflow(psol.result, pos);

		pos = psol.number1.length - 1;
		for (int i = psol.number2.length - 2; i >= 0; --i) {
			pres = psol.number1[pos] * psol.number2[i];

			psol.result[pos + i] += (byte) pres;
			clearOverflow(psol.result, pos + i);
		}

		pos = psol.number2.length - 1;
		for (int i = psol.number1.length - 2; i >= 0; --i) {
			pres = psol.number2[pos] * psol.number1[i];

			psol.result[pos + i] += (byte) pres;
			clearOverflow(psol.result, pos + i);
		}
	}

	private void clearOverflow(byte[] array, int pos) {
		while (array[pos] > 9) {
			int overflow = array[pos] / 10;
			array[pos] = (byte) (array[pos] % 10);
			++pos;
			array[pos] += overflow;
		}
	}

	private PartialSolution nextEmptyPartialSolution(PartialSolution psol) {
		return new PartialSolution(Arrays.copyOf(psol.number1,
				psol.number1.length + 1), Arrays.copyOf(psol.number2,
				psol.number2.length + 1), Arrays.copyOf(psol.result,
				(psol.number1.length + 1) * 2));
	}

	private boolean doesFitTarget(byte[] solution, int start, int end) {
		// signifikante Stellen prüfen
		for (int i = start; i < end; ++i) {
			if (target[i] != solution[i]) {
				return false;
			}
		}
		// über Targetlänge hinausreichende Zahlen prüfen
		for (int i = target.length; i < solution.length; ++i) {
			if (solution[i] > 0) {
				return false;
			}
		}
		// prüfen ob Zahl bereits zu groß ist
		if (solution.length >= target.length) {
			for (int i = target.length - 1; i > 0; --i) {
				if (solution[i] < target[i]) {
					break;
				}
				if (solution[i] > target[i]) {
					return false;
				}
			}
		}
		return true;
	}
}
