package net.mahes.fact.solver;

import java.util.Arrays;

public class PartialSolution implements Comparable<PartialSolution> {

	final byte[] result;

	final byte[] number1;
	final byte[] number2;

	public PartialSolution(byte[] number1, byte[] number2, byte[] result) {
		this.number1 = number1;
		this.number2 = number2;
		this.result = result;
	}

	int significantDigits() {
		return Math.min(number1.length, number2.length);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(number1);
		result = prime * result + Arrays.hashCode(number2);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartialSolution other = (PartialSolution) obj;
		
		if (Arrays.equals(number1, other.number1) && Arrays.equals(number2, other.number2))
			return true;
		
		if (Arrays.equals(number1, other.number2) && Arrays.equals(number2, other.number1))
			return true;
		
		return false;
	}

	@Override
	public int compareTo(PartialSolution other) {
		//return -Integer.compare(result.length, other.result.length);
		return compareInteger(result.length, other.result.length);
	}

	private int compareInteger(int a, int b) {
		return Integer.valueOf(a).compareTo(b);
	}

}
